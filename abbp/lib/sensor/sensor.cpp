#include <Arduino.h>
#include <sensor.h>

float medir_distancia (int trigger, int echo)

{
  float tiempo, distancia;

  digitalWrite (trigger, LOW);
  delayMicroseconds (2);
  digitalWrite (trigger, HIGH);
  delayMicroseconds (10);
  digitalWrite (trigger, LOW);

  tiempo = pulseIn (echo, HIGH);
  distancia = tiempo * 10 /292/2;
  delay (100);
  return (distancia);

}
