#include <Arduino.h>
#include <sensor.h>
#include <display.h>
#include <LiquidCrystal.h>
#include <Wire.h>
#include <motor.h>
#include <math.h>
//#include <puntaje.h>


#define HOME 2 // el final de carrera
#define CHAPA 23
#define DISTANCIA_SENSOR_CHAPA 3.3

LiquidCrystal lcd1 (RS1, EN1, D41, D51, D61, D71);
LiquidCrystal lcd2 (RS2, EN2, D42, D52, D62, D72);

float ext, grados, ady = 0;
const float gradopaso = 1.333;
int paso, flag = 0;
float distanciafinal = 0;
bool turno = false;
int puntaje, pasosdados = 0;
int puntaje1 = 0;
int puntaje2 = 0;



void setup() {
  Serial.begin (9600);

  pinMode (TRIGGER, OUTPUT);
  pinMode (ECHO, INPUT);
  pinMode (ROSA, OUTPUT);
  pinMode (AMARILLO, OUTPUT);
  pinMode (NARANJA, OUTPUT);
  pinMode (HOME, INPUT);

  digitalWrite (ROSA, 0);
  digitalWrite (AMARILLO, 0);
  digitalWrite (NARANJA, 0);

  lcd1.begin (16,2);
  lcd2.begin (16,2);
  lcd1.setCursor (5,0);
  lcd2.setCursor (5,0);
  lcd1.print ("SUERTE");
  lcd2.print ("BUENA");
  delay(3500);
  lcd1.clear();
  lcd2.clear();

  //MOTOR EN 0°
    while (digitalRead (HOME) == HIGH) // cuando no esté pulsado
    {
      //Serial.println (digitalRead(HOME));
      for (int i=ROSA; i<=NARANJA; i++)
      {
     //   Serial.print (i);
        digitalWrite (i, 1);
        delay (5);
        digitalWrite (i, 0);
      }
    }
     //NO OLVIDAR SACAR COMENTARIO
}

void loop()
{
  //ESCRIBIR NOMBRES DISPLAY
    lcd1.setCursor (3,0);
    lcd2.setCursor (3,0); //POSICION Y LINEA
    lcd1.print ("DARK SIDE");
    lcd2.print ("LIGHT SIDE");

  //TURNO EN CADA DISPLAY
    lcd1.setCursor (7,1);
    lcd1.print (puntaje1);
    lcd2.setCursor (7,1);
    lcd2.print (puntaje2);

  //SENSAR DISTANCIA
  distanciafinal = medir_distancia (TRIGGER,  ECHO) - DISTANCIA_SENSOR_CHAPA; //llamo a la funcion
    //GIRA PARA ENCONTRAR
    Serial.println (distanciafinal);

    while ((flag != 2) && (paso < 140))
      {
        lcd1.setCursor (15,1);
        lcd1.print (".");
        lcd2.setCursor (15,1);
        lcd2.print (".");

      // para 180°
        paso_derecha (1);
        paso++;
        Serial.println (paso);
        distanciafinal = medir_distancia (TRIGGER,  ECHO);
        if (distanciafinal < CHAPA)
        {
          if (flag == 0)
          {
            flag = 1;
          }
          else
          {
            flag = 2;
          }
        }
      }
        //ENCONCTRÓ DARDO
          //MIDO DISTANCIA Y GRADOS = PUNTAJES
    if ((distanciafinal < CHAPA) && (paso < 140))// cuando ya hay un dardo y por lo tanto la distancia es menor a 24
      {
        Serial.println (ady);
        ext = gradopaso * paso;
        if (ext > 90)
          {
          grados =  ext - 90;
          }
        else
          {
          grados = 90 - ext;
          }
        ady = cos(grados) * distanciafinal;

          if (ady<0)
          {
            ady = ady * (-1);
          }


          if (ady <= 9)
            {
            puntaje = 10;
            }
          else if ((ady > 9) && (ady <= 12))
            {
            puntaje = 50;
            }
          else if ((ady > 12) && (ady <= 14))
            {
            puntaje = 70;
            }
          else if ((ady > 14) && (ady <= 15))
            {
            puntaje = 100;
            }
          else if ((ady > 15) && (ady <= 17))
            {
            puntaje = 70;
            }
          else if ((ady > 17) && (ady <= 20))
            {
            puntaje = 50;
            }
          else if ((ady>20) && (ady<=25))
            {
            puntaje = 10;
            }
          else
          {
            puntaje = 0;
          }


        if (turno==false)
        {
          puntaje1 = puntaje + puntaje1;
        }
        else
        {
          puntaje2 = puntaje + puntaje2;
        }

        turno=!turno;
        delay (5000);
        flag = 0;
      }

        while (digitalRead (HOME) == HIGH) // cuando no esté pulsado
        {
          for (int i=ROSA; i<=NARANJA; i++)
          {
         //   Serial.print (i);
            digitalWrite (i, 1);
            delay (5);
            digitalWrite (i, 0);
          }
        }

        if (digitalRead(HOME) == LOW)
        {
          paso = 0;
        }
        /*
hola anto, buenos diassss.
espero que disfrutes este jueves/viernes (no se cuando lo vas a leer)
con mucho amor, oli
*/

}
