#include <Arduino.h>
#include <LiquidCrystal.h>
const int EchoPin = 47;
const int TriggerPin = 45;
float ping (int, int);
#define RS1 5
#define EN1 4
#define D41 8
#define D51 3
#define D61 7
#define D71 6
/*
#define RS2 44
#define EN2 47
#define D42 48
#define D52 46
#define D62 22
#define D72 49
#define AZUL 53
#define ROSA 52
#define AMARILLO 51
#define NARANJA 50
int pasos = 253;
*/

LiquidCrystal lcd1 (RS1, EN1, D41, D51, D61, D71);
//LiquidCrystal lcd2 (RS2, EN2, D42, D52, D62, D72);
void setup() {
  /*pinMode (ROSA, OUTPUT);
  pinMode (AMARILLO, OUTPUT);
  pinMode (AZUL, OUTPUT);
  pinMode (NARANJA, OUTPUT);

  digitalWrite (ROSA, 0);
  digitalWrite (AMARILLO, 0);
  digitalWrite (AZUL, 0);
  digitalWrite (NARANJA, 0);
*/
  lcd1.begin (16,2);
  //lcd2.begin (16,2);

  Serial.begin(9600);

}

void loop() {

  lcd1.setCursor(1,0);
 float cm = ping(TriggerPin, EchoPin);
 Serial.print("Distancia: ");
 Serial.println(cm);
 delay(1000);
   lcd1.print (cm);

/* lcd1.setCursor (0,8);
lcd1.print ("PUNTAJE2");
lcd2.setCursor (2,0);
lcd2.print ("PUNTAJE1");

/*
// para -180°
for (int j=0; j<=pasos; j++)
{
  for (int i=AZUL; i>(NARANJA-1); i--)
  {
    Serial.println(i);
    digitalWrite (ROSA, 0);
    digitalWrite (AMARILLO, 0);
    digitalWrite (AZUL, 0);
    digitalWrite (NARANJA, 0);
    digitalWrite (i, 1);
    delay (5);
  }
}

digitalWrite (NARANJA, 0);
delay (5000);

for (int j=0; j<=pasos; j++)
{
  for (int i=NARANJA; i<=AZUL; i++)
  {
    Serial.println(i);
    digitalWrite (ROSA, 0);
    digitalWrite (AMARILLO, 0);
    digitalWrite (AZUL, 0);
    digitalWrite (NARANJA, 0);
    digitalWrite (i, 1);
    delay (5);
  }
}

digitalWrite (AZUL, 0);
delay (5000);

*/



}

float ping(int TriggerPin, int EchoPin) {
   float duration, distanceCm;

   digitalWrite(TriggerPin, LOW);  //para generar un pulso limpio ponemos a LOW 4us
   delayMicroseconds(4);
   digitalWrite(TriggerPin, HIGH);  //generamos Trigger (disparo) de 10us
   delayMicroseconds(10);
   digitalWrite(TriggerPin, LOW);

   duration = pulseIn(EchoPin, HIGH);  //medimos el tiempo entre pulsos, en microsegundos

   distanceCm = duration * 10 / 292/ 2;   //convertimos a distancia, en cm
   return distanceCm;

}
