#include <lib.h>
// cuerpos

float medir_distancia (int trigger, int echo)

{
  float tiempo, distancia;

  digitalWrite (trigger, LOW);
  delayMicroseconds (2);

  digitalWrite (trigger, HIGH);
  delayMicroseconds (10);

  digitalWrite (trigger, LOW);

  tiempo = pulseIn (echo, HIGH);
  distancia = tiempo * 0.5 * 0.0343;
  delay (60);

  return (distancia);

}

/*
void encender_led (float distancia)
{
  if (distancia > 0.50)
  {
    digitalWrite (PIN1, HIGH);
    digitalWrite (PIN2, LOW);
    digitalWrite (PIN3, LOW);
  }
  else if (distancia > 1)
  {
    digitalWrite (PIN1, HIGH);
    digitalWrite (PIN2, HIGH);
    digitalWrite (PIN3, LOW);
  }

  else if (distancia > 1.50)
  {
    digitalWrite (PIN1, HIGH);
    digitalWrite (PIN2, HIGH);
    digitalWrite (PIN3, HIGH);
  }
}
*/
